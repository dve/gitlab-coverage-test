package de.bdr.test;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestAClass {
    @Test
    void aTest() {
        AClass a = new AClass();

        assertEquals(a.doSomething(200), 100);
    }

}
