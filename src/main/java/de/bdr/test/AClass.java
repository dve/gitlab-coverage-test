package de.bdr.test;

public class AClass {
    public int doSomething(int a) {
        if (a == 200) {
            return a / 2;
        } else if (a < 200) {
            return a / 3;
        } else {
            return a / 4;
        }
    }
}
